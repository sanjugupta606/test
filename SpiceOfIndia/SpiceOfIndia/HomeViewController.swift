//
//  HomeViewController.swift
//  SpiceOfIndia
//
//  Created by Sanjay Sah on 12/5/21.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    let itemsArray = ["Vegie Biryani", "Aalo Gobi","Chicken Biryani","Mutton Biryani","Veg Pulao"]

    var menuModel: MenuDataModel?
    var categoryIndex = 0
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var headerButtonView: UIView!
    @IBOutlet weak var menuTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchmenuData()
        createMenuHeaderButtons()
        // Do any additional setup after loading the view.
    }
    
    func createMenuHeaderButtons(){
        if menuModel != nil
        {
            
            var xValue : CGFloat = 10.0
            
        for (index, obj) in menuModel!.menuData!.enumerated() {
            
            let menuButton = CustomButton(type: .custom)
            let wd = (obj.categoryTitle ?? "").width(withHeight: 40.0, font: UIFont(name: "HelveticaNeue-Medium", size: 17.0)!) + 16.0
            menuButton.frame = CGRect(x: xValue, y: 10, width: wd, height: 40)
            menuButton.setTitle(obj.categoryTitle ?? "", for: .normal)
            menuButton.layer.cornerRadius = 20
            menuButton.titleLabel?.font = UIFont(name: "HelveticaNeue-Medium", size: 17.0)
            menuButton.backgroundColor = (index == 0) ? UIColor(hexString: themeColor) : .lightGray
            scrollView.addSubview(menuButton)
            menuButton.tag = index
            menuButton.addTarget(self, action: #selector(menuButtonAction(btn:)), for: .touchUpInside)
            xValue += (wd + 8)
        }
            xValue += 2
            var frame = headerButtonView.frame
            frame.size.width = xValue
            headerButtonView.frame = frame
            
            scrollView.contentSize = CGSize(width: xValue, height: 40)
        
        }
    }
    
    
    @objc func menuButtonAction(btn : CustomButton)
    {
        categoryIndex = btn.tag
        
        if btn.tag != 0
        {
            scrollView.setContentOffset(CGPoint(x: btn.frame.midX-scrollView.frame.size.width/2, y: 0), animated: true)
        }
        else
        {
            scrollView.setContentOffset(CGPoint(x: btn.frame.origin.x-10, y: 0), animated: true)
        }
        
        for subView in scrollView.subviews
        {
            print(subView)
            if subView.isKind(of: CustomButton.self)
            {
                subView.backgroundColor = .lightGray
                if subView.tag == btn.tag
                {
                    btn.backgroundColor = UIColor(hexString: themeColor)
                }
            }
            
        }
        menuTableView.reloadData()
        
    }
    
    func fetchmenuData() {
         menuModel = Helper.shared.getMenuData()
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
//        if let newArray =  menuModel?.menuData
//        {
//            return newArray.
//        }
//
        return  1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let sectionArray = menuModel?.menuData?[categoryIndex].categoryList
        {
            return sectionArray.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerView = CustomHeaderView.loadFromNib() as? CustomHeaderView
        let title = menuModel?.menuData?[categoryIndex].categoryTitle
        headerView?.titleLabel?.text = title
        
        return headerView ?? UIView()// return xib view from file (custom header view)
        
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as? MenuTableViewCell
        let categoryListArray = menuModel?.menuData?[categoryIndex].categoryList
        let menuObj = categoryListArray?[indexPath.row]
        
        cell?.nameLabel.text = menuObj?.title
        cell?.descLabel.text = menuObj?.categoryListDescription
        cell?.priceLabel.text = "$\(menuObj?.price ?? 0.0)"
        cell?.itemImageView.image = UIImage(named: menuObj?.itemImage ?? "veg-biryani-recipe")
        setRating(rating: menuObj?.rating ?? 1, cell: cell)
        return cell ?? UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let categoryListArray = menuModel?.menuData?[categoryIndex].categoryList
        let menuObj = categoryListArray?[indexPath.row]
        let detailvc = self.storyboard?.instantiateViewController(withIdentifier:"DetailedViewController") as? DetailedViewController
        detailvc?.menuObj = menuObj
        self.navigationController?.pushViewController(detailvc!, animated: true)
    }
    
    
    func setRating(rating:Int, cell:MenuTableViewCell?){
        
        for index in 0..<cell!.ratingImagesArray.count {
            
            if (index+1) <= rating{
                cell?.ratingImagesArray[index].image = UIImage(named: "rating_star_filled")
            }
            else {
                cell?.ratingImagesArray[index].image = UIImage(named: "rating")
            }
        }
        
        
    }

}
