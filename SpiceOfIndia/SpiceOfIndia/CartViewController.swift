//
//  CartViewController.swift
//  SpiceOfIndia
//
//  Created by Sanjay Sah on 12/5/21.
//

import UIKit

class CartViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
   

    @IBOutlet weak var chekoutBtn: CustomButton!
    @IBOutlet weak var cartTableView: UITableView!
    @IBOutlet weak var orderSummary: UILabel!
    var total: Double = 0.0
    @IBAction func checkOut(_ sender: CustomButton) {
        
        for var model in Helper.shared.selectedItemsArray
        {
        model.totalPrice = total
         DatabaseManager.shared.saveOrder(model: model)
            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if Helper.shared.selectedItemsArray.count>0
        {
            chekoutBtn.isEnabled = true
            
        }
        else
        {
            chekoutBtn.isEnabled = false
        }
        displayOrderSummaryTotalAmount()
        cartTableView.reloadData()
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        // Do any additional setup after loading the view.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Helper.shared.selectedItemsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableViewCell", for: indexPath) as? CartTableViewCell
        let obj = Helper.shared.selectedItemsArray[indexPath.row]
        
        cell?.itemImageView.image = UIImage(named: obj.itemImage ?? "")
        cell?.itemName.text  = obj.title
        cell?.itemQuantiy.attributedText = Helper.getAttributedString(labelStr: "Quantity  ", font: UIFont.regular(), valueStr:"\(String(describing: obj.quantity ?? 1))", valueFont: UIFont.medium(size: 17.0))
        cell?.itemPrice.attributedText = Helper.getAttributedString(labelStr: "Price  ", font: UIFont.regular(), valueStr:"$\(String(describing: obj.price ?? 0.0))", valueFont: UIFont.medium(size: 18.0), valueColor: "#00b300")
        
        return cell ?? UITableViewCell()
    }
    
    func displayOrderSummaryTotalAmount()
    {
        
        for object in Helper.shared.selectedItemsArray
        {
            total += ((object.price ?? 0.0)*Double((object.quantity ?? 1)))
        }
        
        print(total)
        
        orderSummary.attributedText = Helper.getAttributedString(labelStr: "Order Summary Total   ", font: UIFont.medium(size: 18.0), valueStr:"$\(String(describing: total))", valueFont: UIFont.medium(size: 20.0), valueColor: themeColor)
        
    }
    
    func showAlert(title:String,message:String)
    {
        // We created UI alert controll to display
        let alertControl = UIAlertController(title: title, message: message, preferredStyle: .alert)
        // We created this button to dismiss from pop up screen.
        let okBtn = UIAlertAction(title: "OK", style: .default) { btn in
            
        }
        // This is adding button on alert pop up
        alertControl.addAction(okBtn)
        // this is presenting/displaying
        self.present(alertControl, animated: true, completion: nil)
        
    }

    
    }


