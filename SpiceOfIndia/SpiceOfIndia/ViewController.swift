//
//  ViewController.swift
//  SpiceOfIndia
//
//  Created by Sanjay Sah on 11/25/21.
//

import UIKit
import GoogleSignIn

class ViewController: UIViewController {
      
   
    @IBAction func googleSignIn(_ sender: UIButton) {
        
        GIDSignIn.sharedInstance.signIn(with: signInConfig, presenting: self) { user, error in
            guard error == nil else { return }
            
            if user != nil
            {
                print(user?.profile?.name)
                print(user?.profile?.email)
                self.visithomeScreen()
            }

            // If sign in succeeded, display the app's main content View.
          }
    }
    
    func visithomeScreen(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "TabBarViewController") as? TabBarViewController
        // Setting root view controller to the app instead of push codes and to avoid to keep previous view controllers in the memory
        let appdel = UIApplication.shared.delegate as? AppDelegate
        appdel?.window?.rootViewController = homeVC
        appdel?.window?.makeKeyAndVisible()
        
    }
    
    @IBAction func LoginVC(_ sender: CustomButton) {
        let loginVC = self.storyboard?.instantiateViewController(identifier: "LoginViewController") as? LoginViewController
            self.navigationController?.pushViewController(loginVC!, animated: true)

    }
    
    @IBAction func signUpClicked(_ sender: CustomButton) {
        let signUp = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController
        self.navigationController?.pushViewController(signUp!, animated: true)
    }
    
}
