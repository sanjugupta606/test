//
//  UIViewExtension.swift
//  SpiceOfIndia
//
//  Created by Sanjay Sah on 12/12/21.
//

import UIKit

extension UIView
{
    class func loadFromNib<T: UIView>() -> T {
           return Bundle.main.loadNibNamed(String(describing: self), owner: nil, options: nil)?[0] as! T
       }
    
    class func loadFromNibWith<T: UIView>(tag: Int) -> T {
        var requiredView  = T()
           if let viewArray = Bundle.main.loadNibNamed(String(describing: self), owner: nil, options: nil) as? [T]
            {
               for view in viewArray
               {
                   if view.tag == tag
                   {
                       requiredView = view
                       break
                   }
               }
               return requiredView
           }
        return requiredView
       }
}
