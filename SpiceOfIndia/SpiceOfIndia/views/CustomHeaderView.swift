//
//  CustomHeaderView.swift
//  SpiceOfIndia
//
//  Created by Sanjay Sah on 12/12/21.
//

import UIKit

class CustomHeaderView: UIView {
    
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
