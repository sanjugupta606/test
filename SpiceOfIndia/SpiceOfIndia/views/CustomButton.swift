//
//  CustomButton.swift
//  SpiceOfIndia
//
//  Created by Sanjay Sah on 11/25/21.
//

import Foundation
import UIKit

@IBDesignable
class CustomButton: UIButton {
    @IBInspectable var cornerRadiusValue: CGFloat = 0.0 {
        didSet {
            setUpView()
        }
    }
  
  @IBInspectable public var borderColor : UIColor = UIColor.clear {
         didSet {
             self.layer.borderColor = borderColor.cgColor
         }
     }
  
  @IBInspectable public var btnBorderWidth : CGFloat = 0.0 {
         didSet {
             self.layer.borderWidth = btnBorderWidth
         }
     }
  
    override func awakeFromNib() {
        super.awakeFromNib()
        setUpView()
    }
  
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        setUpView()
    }
  
    func setUpView() {
        self.layer.cornerRadius = self.cornerRadiusValue
        self.clipsToBounds = true
    }
}

