//
//  MoreViewController.swift
//  SpiceOfIndia
//
//  Created by Sanjay Sah on 12/5/21.
//

import UIKit
import Social

class MoreViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
   
    let optionArray = [["iconName":"profile", "title":"Profile"],["iconName":"about", "title":"About"],["iconName":"share", "title":"Share on Media"]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return optionArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoreTableViewCell", for: indexPath) as? MoreTableViewCell
        let someDict = optionArray[indexPath.row]
        cell?.iconImageView.image = UIImage(named: someDict["iconName"] ?? "")
        cell?.optionTitle.text = someDict["title"]
        
        return cell ?? UITableViewCell()
        
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 0
        {
            //profile
            let profile = storyboard?.instantiateViewController(withIdentifier: "ProfileViewController") as? ProfileViewController
            self.navigationController?.pushViewController(profile!, animated: true)
            
        } else if indexPath.row == 2 {
            let slComposer = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            
            slComposer?.setInitialText("test spice of india")
            slComposer?.add(UIImage(named: "Spice Of India"))
            let completionHandler = {(result:SLComposeViewControllerResult) -> () in
                        slComposer?.dismiss(animated: true, completion: nil)
                        switch(result){
                        case .cancelled:
                            print("User canceled", terminator: "")
                        case .done:
                            print("User tweeted", terminator: "")
                        @unknown default:
                            fatalError()
                        }
                }
            slComposer?.completionHandler = completionHandler
            
            self.present(slComposer!, animated: true, completion: nil)
            
            
        }
        else if indexPath.row == 1 {
            let aboutVC = storyboard?.instantiateViewController(withIdentifier: "AboutViewController") as? AboutViewController
            self.navigationController?.pushViewController(aboutVC!, animated: true)
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
