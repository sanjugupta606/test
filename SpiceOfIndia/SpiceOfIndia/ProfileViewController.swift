//
//  ProfileViewController.swift
//  SpiceOfIndia
//
//  Created by Sanjay Sah on 1/3/22.
//

import UIKit

class ProfileViewController: UIViewController {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var mobileLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        profileImage.image = UIImage(named: "")
        nameLabel.text = "Sanjay"
        mobileLabel.text = "7207551272"
        addressLabel.text = "California"
        emailLabel.text = "firebase734@gmail.com"
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
