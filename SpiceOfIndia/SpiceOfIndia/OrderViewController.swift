//
//  OrderViewController.swift
//  SpiceOfIndia
//
//  Created by Sanjay Sah on 12/5/21.
//

import UIKit
import CoreData

class OrderViewController: UIViewController , UITableViewDataSource, UITableViewDelegate {
       

        @IBOutlet weak var orderTableView: UITableView!
        @IBOutlet weak var orderSummary: UILabel!
        var selectedSegmentIndex: Int = 0
        var selectedIndex: Int = 0
        var orderListArray = [OrderModel]()
        var bookModelList : [BookModel]?
    
        @IBAction func checkOut(_ sender: CustomButton) {
            
            for model in Helper.shared.selectedItemsArray
            {
        
             DatabaseManager.shared.saveOrder(model: model)
                
            }
            
        }
        
        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)
           // displayOrderSummaryTotalAmount()
            orderListArray = DatabaseManager.shared.fetchData()
            setUpTableHeaderView()
            orderTableView.reloadData()
            
        }
        
        override func viewDidLoad() {
            super.viewDidLoad()
        
            
            // Do any additional setup after loading the view.
        }
        
    func setUpTableHeaderView(){
        
        let headerView = CustomHeaderView.loadFromNibWith(tag: 102) as? CustomHeaderView
        headerView?.segmentedControl.selectedSegmentIndex = selectedSegmentIndex
        headerView?.segmentedControl.addTarget(self, action: #selector(segmetnAction(segment: )), for: .valueChanged)
        orderTableView.tableHeaderView = headerView
        
    }

  @objc  func segmetnAction(segment: UISegmentedControl)
    {
        selectedSegmentIndex = segment.selectedSegmentIndex
        
        if segment.selectedSegmentIndex == 1
        {
            if bookModelList == nil
            {
                bookModelList = DatabaseManager.shared.fetchTableBookingList()
            }
        }
        
        
        orderTableView.reloadData()
    }
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if selectedSegmentIndex == 1
            {
                return bookModelList?.count ?? 0
            }
            
            return orderListArray.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableViewCell", for: indexPath) as? CartTableViewCell
            if selectedSegmentIndex == 0
            {
            let obj = orderListArray[indexPath.row]
            
            cell?.itemImageView.image = UIImage(named: obj.itemImage ?? "")
            cell?.itemName.text  = obj.itemName
            cell?.itemQuantiy.attributedText = Helper.getAttributedString(labelStr: "Quantity  ", font: UIFont.regular(), valueStr:"\(String(describing: obj.quantity ?? 1))", valueFont: UIFont.medium(size: 17.0))
            cell?.itemPrice.attributedText = Helper.getAttributedString(labelStr: "Price  ", font: UIFont.regular(), valueStr:"$\(String(describing: obj.price ?? 0.0))", valueFont: UIFont.medium(size: 18.0), valueColor: "#00b300")
            cell?.orderAmount.attributedText = Helper.getAttributedString(labelStr: "Price  ", font: UIFont.regular(), valueStr:"$\(String(describing: obj.orderAmount ?? 0.0))", valueFont: UIFont.medium(size: 18.0), valueColor: "#00b300")
            
            
            }
            else
            {
                let obj = bookModelList?[indexPath.row]
                
                cell?.itemImageView.image = UIImage(named:  "vegiekorma")
                cell?.itemName.text  = ""
                cell?.itemQuantiy.attributedText = Helper.getAttributedString(labelStr: "No persons: ", font: UIFont.regular(), valueStr:"\(String(describing: obj?.persons ?? 0))", valueFont: UIFont.medium(size: 17.0))
                cell?.itemPrice.text = ""
                cell?.orderID.text = ""
                cell?.orderAmount.attributedText = Helper.getAttributedString(labelStr: "Checkin Date: ", font: UIFont.regular(), valueStr:"\(String(describing: obj?.checkInDate ?? ""))", valueFont: UIFont.medium(size: 18.0), valueColor: "#00b300")
                
               
            }
            return cell ?? UITableViewCell()
        }
        
       
        

}
