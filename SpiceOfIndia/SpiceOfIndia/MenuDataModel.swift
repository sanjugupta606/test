//
//  MenuDataModel.swift
//  SpiceOfIndia
//
//  Created by Sanjay Sah on 12/11/21.
//

import Foundation


// MARK: - MenuDataModel
struct MenuDataModel: Codable {
    let menuData: [MenuDatum]?
}

// MARK: - MenuDatum
struct MenuDatum: Codable {
    let categoryTitle: String?
    let categoryList: [CategoryList]?
}

// MARK: - CategoryList
struct CategoryList: Codable, Equatable {
    static func == (lhs: CategoryList, rhs: CategoryList) -> Bool {
        return true
    }
    
    let title, categoryListDescription, itemImage: String?
    let rating, offerPrice: Int?
    let price: Double?
    var quantity: Int?
    var totalPrice : Double?
    let subCategory: [SubCategory]?

    enum CodingKeys: String, CodingKey {
        case title
        case categoryListDescription = "description"
        case itemImage, price, rating, offerPrice, subCategory
    }
}

// MARK: - SubCategory
struct SubCategory: Codable {
    let title: String?
}

