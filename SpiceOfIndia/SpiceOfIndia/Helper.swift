//
//  Helper.swift
//  SpiceOfIndia
//
//  Created by Sanjay Sah on 12/8/21.
//

import Foundation
import UIKit
    // this helper class swift file is made to verify email and phone validation field whether it is in correct order or not. 
class Helper
{
  static let shared = Helper()
    var selectedItemsArray = [CategoryList]()
  
  class func isValidEmail(email: String) -> Bool {
      let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
      var valid = NSPredicate(format: "SELF MATCHES %@", emailRegex).evaluate(with: email)
//      if valid {
//          valid = !email.contains("Invalid email id")
//      }
      return valid
  }
 
    class   func getAttributedString(labelStr: String, font: UIFont, valueStr: String, valueFont: UIFont, valueColor: String? = "#000000") ->NSMutableAttributedString
    {
        let atrStr = NSMutableAttributedString()
        atrStr.append(NSAttributedString(string: labelStr, attributes: [.font : font]))
        atrStr.append(NSAttributedString(string: valueStr, attributes: [.font : valueFont, .foregroundColor : UIColor(hexString: valueColor!) ]))
        return atrStr
    }
  class func isValidPhone(phone: String) -> Bool {
          let phoneRegex = "^[0-9+]{0,1}+[0-9]{5,16}$"
          let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneRegex)
          return phoneTest.evaluate(with: phone)
      }
    // reading menudatamodel file and calling from json
    func getMenuData()->MenuDataModel?{
        
        if let url = Bundle.main.url(forResource: "menu", withExtension: "json") {
                do {
                    let data = try Data(contentsOf: url)
                    let decoder = JSONDecoder()
                    let model = try decoder.decode(MenuDataModel.self, from: data)
                    return model
                } catch {
                    print("error:\(error)")
                }
            }
            return nil
        }
  
}

let themeColor = "#FF2600"

extension UIColor {
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt64()
        Scanner(string: hex).scanHexInt64(&int)
        let a, r, g, b: UInt64
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
}

extension UILabel {
    func textHeight(withWidth width: CGFloat) -> CGFloat {
        guard let text = text else {
            return 0
        }
        return text.height(withWidth: width, font: font)
    }
    
    func textWidth(withHeight height: CGFloat) -> CGFloat {
        guard let text = text else {
            return 0
        }
        return text.width(withHeight: height, font: font)
    }


    func attributedTextHeight(withWidth width: CGFloat) -> CGFloat {
        guard let attributedText = attributedText else {
            return 0
        }
        return attributedText.height(withWidth: width)
    }
}

extension String {
    func height(withWidth width: CGFloat, font: UIFont) -> CGFloat {
        let maxSize = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let actualSize = self.boundingRect(with: maxSize, options: [.usesLineFragmentOrigin], attributes: [.font : font], context: nil)
        return actualSize.height
    }
    
    func width(withHeight height: CGFloat, font: UIFont) -> CGFloat {
        let maxSize = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        let actualSize = self.boundingRect(with: maxSize, options: [.usesLineFragmentOrigin], attributes: [.font : font], context: nil)
        return actualSize.width
    }
}

extension NSAttributedString {
    func height(withWidth width: CGFloat) -> CGFloat {
        let maxSize = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let actualSize = boundingRect(with: maxSize, options: [.usesLineFragmentOrigin], context: nil)
        return actualSize.height
    }
}

//HelveticaNeue-Bold,
//HelveticaNeue-CondensedBlack,
//HelveticaNeue-Medium,
//HelveticaNeue,
//HelveticaNeue-Light,
//HelveticaNeue-CondensedBold,
//HelveticaNeue-LightItalic,
//HelveticaNeue-UltraLightItalic,
//HelveticaNeue-UltraLight,
//HelveticaNeue-BoldItalic,
//HelveticaNeue-Italic
extension UIFont
{
    class func regular(size: CGFloat? = 16.0) ->UIFont
    {
        UIFont(name: "HelveticaNeue", size: size!)!
    }
    class func medium(size: CGFloat? = 16.0) ->UIFont
    {
        UIFont(name: "HelveticaNeue-Medium", size: size!)!
    }
    class func bold(size: CGFloat? = 16.0) ->UIFont
    {
        UIFont(name: "HelveticaNeue-Bold", size: size!)!
    }
    class func italic(size: CGFloat? = 16.0) ->UIFont
    {
        UIFont(name: "HelveticaNeue-Italic", size: size!)!
    }
}
//extension String {
//
//func width(constraintedheight width: CGFloat, font: UIFont) -> CGFloat {
//    let label =  UILabel(frame: CGRect(x: 0, y: 0, width: width, height: .greatestFiniteMagnitude))
//    label.numberOfLines = 0
//    label.text = self
//    label.font = font
//    label.sizeToFit()
//
//    return label.frame.height
// }
//}


