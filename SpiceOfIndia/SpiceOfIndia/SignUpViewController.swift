//
//  SignUpViewController.swift
//  SpiceOfIndia
//
//  Created by Sanjay Sah on 11/27/21.
//

import UIKit

class SignUpViewController: UIViewController {
    
    @IBAction func backButtonClick(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var userName: UITextField!
    
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var confirmPassword: UITextField!
    
    @IBOutlet weak var emailField: UITextField!
    
    @IBOutlet weak var phoneNumberField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func signUpAction(_ sender: CustomButton) {
        
        let tuple = validateFields()
        
        if tuple.1 != ""   // .1 here means string(second value message) calling
        {
            showAlert(title: "Empty Field!", message: tuple.1)
        }
        else if passwordField.text != confirmPassword.text
        {
            showAlert(title: "Password Validation!", message: "Password and confirm password should be same")
        }
        else if !Helper.isValidEmail(email: emailField.text ?? "")
        {
            showAlert(title: "Email Validation!", message: "Please enter valid email")
        }
        else if Helper.isValidPhone(phone: phoneNumberField.text ?? "")
        {
            showAlert(title: "Phone Validation!", message: "Enter valid phone number")
        }
        else
        {
            //signup
        }
        
       }
    //making function to call and validate fields
    func validateFields()->(Int,String){
        var tuple = (0,"")
        
        // checking whether all fields are filled or  not
        if userName.text == "" && passwordField.text == "" && confirmPassword.text == "" && emailField.text == "" && phoneNumberField.text == ""
        {
            tuple = (0,"Please Enter All fields")
        }else if userName.text == "" || userName.text?.isEmpty == true // checking whether username is filled or empty
        {
            tuple = (0,"Enter your username")
        }
        else if passwordField.text == "" || passwordField.text?.isEmpty == true // checking whether password field is empty or field
        {
            tuple = (1,"Enter your password")
        }
        else if confirmPassword.text == "" || confirmPassword.text?.isEmpty == true
        {
            tuple = (2,"Enter Valid Password")
        }
        else if phoneNumberField.text == "" || phoneNumberField.text?.isEmpty == true
        {
            tuple = (3,"Enter Valid Phone Number")
        }
        else if emailField.text == "" || emailField.text?.isEmpty == true
        {
            tuple = (4,"Enter Valid Email")
        }
       return tuple
    }
    
    func showAlert(title:String,message:String)
    {
        // We created UI alert controll to display
        let alertControl = UIAlertController(title: title, message: message, preferredStyle: .alert)
        // We created this button to dismiss from pop up screen.
        let okBtn = UIAlertAction(title: "OK", style: .default) { btn in
            
        }
        // This is adding button on alert pop up
        alertControl.addAction(okBtn)
        // this is presenting/displaying
        self.present(alertControl, animated: true, completion: nil)
        
    
    }
    
}
