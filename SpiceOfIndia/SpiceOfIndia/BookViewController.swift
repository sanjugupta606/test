//
//  BookViewController.swift
//  SpiceOfIndia
//
//  Created by Sanjay Sah on 12/5/21.
//

import UIKit
import CoreData

class BookViewController: UIViewController {
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var bookDate: UITextField!
    @IBOutlet weak var person: UITextField!
    var selectedDate : Date?
    @IBOutlet weak var bookTime: UITextField!
    
    
    @IBAction func bookAction(_ sender: CustomButton) {
        
        if let count = person.text,  Int(count)!>0 {
            
        
            DatabaseManager.shared.bookTable(membersCount: Int(count)!, dateStr: getStringFromDate(date: datePicker.date))
        }
        else
        {
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func datePickerAction(_ sender: UIDatePicker) {
        
        selectedDate = sender.date
        
        self.dismiss(animated: true, completion: nil)
    }
    
    func getStringFromDate(date: Date)->String
    {
        let dateFormat = DateFormatter()
        dateFormat.dateFormat = "MMM dd, yyyy hh:mm a"
        
        return dateFormat.string(from: date)
        
    }

  

}
