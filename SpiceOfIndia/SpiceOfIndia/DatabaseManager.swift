//
//  DatabaseManager.swift
//  SpiceOfIndia
//
//  Created by Sanjay Sah on 12/22/21.
//

import UIKit
import CoreData

class DatabaseManager: NSObject {
    
    static let shared = DatabaseManager()

    
    @discardableResult func saveOrder(model: CategoryList) ->Bool
    {
        let appdel = UIApplication.shared.delegate as? AppDelegate
        let context = appdel?.persistentContainer.viewContext
        let orderModel = NSEntityDescription.insertNewObject(forEntityName: "OrderModel", into: context!) as? OrderModel
        orderModel?.itemName = model.title
        orderModel?.orderAmount = model.totalPrice ?? 0.0
        orderModel?.price = model.price ?? 0.0
        let fetchDataArray = fetchData()
        orderModel?.orderId = Int32(fetchDataArray.count ?? 1) + 1
        orderModel?.quantity = Int32(model.quantity ?? 1)
        
        appdel?.saveContext()
        
        
        return true
    }
    
    func fetchData() -> [OrderModel]
    {
        let appdel = UIApplication.shared.delegate as? AppDelegate
        let context = appdel?.persistentContainer.viewContext
        let fetchrequest = NSFetchRequest<NSFetchRequestResult>(entityName: "OrderModel")
        
        let resultArray = try? context?.fetch(fetchrequest) as? [OrderModel]
        
        return resultArray ?? []
    }
    
    @discardableResult func bookTable(membersCount:Int, dateStr: String) ->Bool
    {
        let appdel = UIApplication.shared.delegate as? AppDelegate
        let context = appdel?.persistentContainer.viewContext
        let orderModel = NSEntityDescription.insertNewObject(forEntityName: "BookModel", into: context!) as? BookModel
        orderModel?.persons = Int16(membersCount)
        orderModel?.checkInDate = dateStr
       
        let fetchDataArray = fetchTableBookingList()
        orderModel?.orderId = Int32(fetchDataArray.count ?? 1) + 1
        
        
        appdel?.saveContext()
        
        
        return true
    }
    
    func fetchTableBookingList() -> [BookModel]
    {
        let appdel = UIApplication.shared.delegate as? AppDelegate
        let context = appdel?.persistentContainer.viewContext
        let fetchrequest = NSFetchRequest<NSFetchRequestResult>(entityName: "BookModel")
        
        let resultArray = try? context?.fetch(fetchrequest) as? [BookModel]
        print("table list oarray \(resultArray)")
        return resultArray ?? []
    }
    
}
