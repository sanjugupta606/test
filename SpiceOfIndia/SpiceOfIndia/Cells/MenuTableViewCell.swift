//
//  MenuTableViewCell.swift
//  SpiceOfIndia
//
//  Created by Sanjay Sah on 12/6/21.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    //itemImageView
    
    @IBOutlet weak var itemImageView: UIImageView!
    //nameLabel
    
    @IBOutlet weak var nameLabel: UILabel!
    //descLabel
    
    @IBOutlet weak var descLabel: UILabel!
    //priceLabel
    @IBOutlet weak var priceLabel: UILabel!
    
    //stepper
    @IBOutlet weak var stepper: UIStepper!
    
    
    
    // collection outlet
    @IBOutlet var ratingImagesArray: [UIImageView]!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
