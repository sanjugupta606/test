//
//  MoreTableViewCell.swift
//  SpiceOfIndia
//
//  Created by Sanjay Sah on 12/29/21.
//

import UIKit

class MoreTableViewCell: UITableViewCell {

    
    @IBOutlet weak var optionTitle: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
