//
//  OrderTableViewCell.swift
//  SpiceOfIndia
//
//  Created by Sanjay Sah on 12/22/21.
//

import UIKit

class CartTableViewCell: UITableViewCell {

    @IBOutlet weak var itemQuantiy: UILabel!
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var itemName: UILabel!
  
    @IBOutlet weak var orderAmount: UILabel!
    @IBOutlet weak var orderID: UILabel!
    @IBOutlet weak var itemImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
