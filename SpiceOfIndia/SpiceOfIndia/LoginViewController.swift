//
//  Login.swift
//  SpiceOfIndia
//
//  Created by Sanjay Sah on 11/25/21.
//

import UIKit
import QuartzCore

class LoginViewController: UIViewController {
    
    
    @IBAction func backButton(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBOutlet weak var userNameField: UITextField!
    
    
    @IBOutlet weak var passwordField: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userNameField.text = "sanjay"
        passwordField.text = "12345"
        }
        // Do any additional setup after loading the view.
    
    @IBAction func loginClicked(_ sender: CustomButton)
    {
        if userNameField.text == "" || passwordField.text == ""{
            showAlert(title : "Empty Fields!", message: "Enter username and Password")
        }
        else if userNameField.text?.lowercased() == "sanjay" && passwordField.text == "12345"
        {
           print("Login Successful")
            self.visithomeScreen() // this code will make screen to go on home screen
            
        } else {
            
            showAlert(title : "Login Failed!", message: "Enter Valid username or password Password")
        }
        
            
    }
    // We should have to create function code before doing any showAlert work.
    func showAlert(title:String,message:String)
    {
        // We created UI alert controll to display
        let alertControl = UIAlertController(title: title, message: message, preferredStyle: .alert)
        // We created this button to dismiss from pop up screen.
        let okBtn = UIAlertAction(title: "OK", style: .default) { btn in
            
        }
        // This is adding button on alert pop up
        alertControl.addAction(okBtn)
        // this is presenting/displaying
        self.present(alertControl, animated: true, completion: nil)
        
    }

    
    func visithomeScreen(){
        let homeVC = self.storyboard?.instantiateViewController(withIdentifier: "TabBarViewController") as? TabBarViewController
        // Setting root view controller to the app instead of push codes and to avoid to keep previous view controllers in the memory
        let appdel = UIApplication.shared.delegate as? AppDelegate
        appdel?.window?.rootViewController = homeVC
        appdel?.window?.makeKeyAndVisible()
        
    }

}

