//
//  DetailedViewController.swift
//  SpiceOfIndia
//
//  Created by Sanjay Sah on 12/18/21.
//

import UIKit


extension Array where Element: Equatable {

    // Remove first collection element that is equal to the given `object`:
    mutating func remove(object: Element) {
        guard let index = firstIndex(of: object) else {return}
        remove(at: index)
    }

}

enum QuantityType: Int
{
    case up
    case down
}

class DetailedViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
   
    
    var menuObj : CategoryList?
    var quantity : Int = 1
    
    @IBOutlet weak var addCartButton: CustomButton!
    @IBOutlet weak var quantityValueLabel: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

       // self.navigationItem.backBarButtonItem?.title = "Back"
        updatePrice()
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func addCartButtonAction(_ sender: CustomButton) {
        
        
        if Helper.shared.selectedItemsArray.contains(where : {$0.title == menuObj?.title})
        {
            let price : Double = menuObj?.price ?? 0.0
            
            menuObj?.totalPrice = Double(quantity)*(price)
            guard let index = Helper.shared.selectedItemsArray.firstIndex(of: menuObj!) else {return}
            
            //Helper.shared.selectedItemsArray.remove
            Helper.shared.selectedItemsArray[index] = menuObj!
        }
        else
        {
            Helper.shared.selectedItemsArray.append(menuObj!)
        }
        
        
    }
    
    
    @IBAction func increaseQuantityAction(_ sender: UIButton) {
       
        updatePriceForQuanity(type: .up)
    }
    
    @IBAction func decreaseQuantityAction(_ sender: UIButton) {
        
        
        updatePriceForQuanity(type: .down)
    }
    
    func updatePriceForQuanity(type : QuantityType)
    {
        if type == .down
        {
            if quantity > 0
            {
                quantity -= 1
            }
        }
        else
        {
            quantity += 1
        }
        
        updatePrice()
    }
    
    func updatePrice()
    {
        quantityValueLabel.text = "\(quantity)"
        menuObj?.quantity = quantity
        addCartButton.setTitle("ADD TO CART  $\(Double(quantity)*(menuObj?.price ?? 0.0)!)", for: .normal)

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailedTableViewCell", for: indexPath) as? DetailedTableViewCell
        cell?.itemNameLabel.text = menuObj?.title
        
        cell?.descLabel.text = menuObj?.categoryListDescription
        cell?.priceLabel.text = "$\(String(describing: menuObj?.price ?? 0.0))"
        cell?.itemImageView.image = UIImage(named: menuObj?.itemImage ?? "")
        setRating(rating: menuObj?.rating ?? 1, cell: cell)
        return cell ?? UITableViewCell()
    }

    func setRating(rating:Int, cell:DetailedTableViewCell?){
        
        for index in 0..<cell!.ratingImagesArray.count {
            
            if (index+1) <= rating{
                cell?.ratingImagesArray[index].image = UIImage(named: "rating_star_filled")
            }
            else {
                cell?.ratingImagesArray[index].image = UIImage(named: "rating")
            }
        }
        
        
    }

}
